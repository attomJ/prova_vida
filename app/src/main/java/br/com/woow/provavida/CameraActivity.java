package br.com.woow.provavida;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.bigid.entities.ValidateObject;
import com.bigid.model.BigID;
import com.bigid.model.BigIdEvent;
import com.bigid.model.Configuration;
import com.bigid.model.LivenessProofResult;

import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import bigid.camerahelper.CameraConnectionFragment;

public class CameraActivity extends AppCompatActivity implements LivenessProofResult {

    public static CameraActivity newInstance() {
        return new CameraActivity();
    }
    
    private static final String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6InJhZmFlbG1lbG9AY3JlZGlsaW5rLmNvbS5iciIsIm5iZiI6MTY0MDczNjQ3OSwiZXhwIjoxOTU1NzM2NDc5LCJpYXQiOjE2NDA3MzY0NzksImlzcyI6IkJpZyBEYXRhIENvcnAuIiwicHJvZHVjdHMiOltdLCJkb21haW4iOiJDUkVESUxJTksifQ.2cORXW_JVFkGjqBaOKAZKX5RJ-H9kSo0zBF8kv7sUxc";

    public CameraActivity(){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        Configuration config = new Configuration();
        config.setImageDrawable(R.id.activity_imageview_logo_credlink);
        config.setBackgroundColor("#FFFFFF");
        config.setWelcomeText("Vamos começar!");
        config.setLastText("Hora da Foto!");
        config.setFaceOutline(true);

        System.out.println("Running " +  CameraActivity.class.getSimpleName());
        //Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        //startActivity(intent);

        ValidateObject object = BigID.init(TOKEN, config);
        if(object.isSuccess()) {
            CameraActivity.newInstance();
            getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_linearLayout, CameraConnectionFragment.newInstance())
                .commit();
        }        

       // finish();
    }

    @Override
    public void returnLivenessProof(BigIdEvent bigIdEvent) {
        if (bigIdEvent.getLivenessEvent() != null) {
            Handler handler = new Handler(this.getMainLooper());
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    final TextView textView = (TextView) findViewById(R.id.activity_txtview_prova_vida);
                    textView.setText(bigIdEvent.getLivenessEvent().getMessage());
                    final ImageView imgView = (ImageView) findViewById(R.id.logo);
                    imgView.setImageBitmap(bigIdEvent.getLivenessEvent().getCustomerLastFrame());
                }
            };
            handler.post(r);
        }
    }
}